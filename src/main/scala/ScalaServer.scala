import cats.effect.{ConcurrentEffect, ContextShift, Timer}
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.Logger

import scala.concurrent.ExecutionContext.global

object ScalaServer {

    def stream[F[_]: ConcurrentEffect: Timer: ContextShift]: Stream[F, Nothing] = {
        for {
            client <- BlazeClientBuilder[F](global).stream
            dataParserAlg = DataParser.impl[F]

            // Combine Service Routes into an HttpApp.
            // Can also be done via a Router if you
            // want to extract a segments not checked
            // in the underlying routes.
            httpApp = (
              ScalaRoute.DataParserRoutes[F](dataParserAlg)
              ).orNotFound

            // With Middlewares in place
            finalHttpApp = Logger.httpApp(true, true)(httpApp)

            exitCode <- BlazeServerBuilder[F]
              .bindHttp(8080, "0.0.0.0")
              .withHttpApp(finalHttpApp)
              .serve
        } yield exitCode
    }.drain
}