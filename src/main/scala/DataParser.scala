import MessageEngine._
import cats.Applicative
import io.circe.{Encoder, Json}
import org.http4s.EntityEncoder
import org.http4s.circe.jsonEncoderOf
import org.json4s._
import org.json4s.native.JsonMethods._

trait DataParser[F[_]]{
    def JsonParser(origin: DataParser.OriginData): DataParser.Querying
}


object DataParser {

//    implicit def apply[F[_]](implicit ev: Calculator[F]): Calculator[F] = ev

    final case class OriginData(originData: String) extends AnyVal
    final case class ResultData(resultData: String) extends AnyVal

    final case class Querying(result: MessageObject) extends AnyVal

    object Querying {
        //encode the respond to Json
        implicit val resultEncoder: Encoder[Querying] = new Encoder[Querying] {
            //            final def apply(a: Querying): Json = Json.obj(
            //                ("message", Json.fromString(a.result)),
            //            )
            final def apply(a: Querying): Json = Json.obj(
                ("code", Json.fromString(a.result.messageCode)),
                ("message", Json.fromString(a.result.messageHeader)),
            )
        }

        implicit def resultEntityEncoder[F[_]: Applicative]: EntityEncoder[F, Querying] =
            jsonEncoderOf[F, Querying]

    }

    def impl[F[_]: Applicative]: DataParser[F] = new DataParser[F]{
        def JsonParser(origin: DataParser.OriginData): Querying = {
            val json: JValue = parse(origin.originData)

            Querying(ResultMessage(json.toString))
        }
    }

}
