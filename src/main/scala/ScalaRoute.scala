import cats.effect.Sync
import cats.implicits._
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

//package object http {
//    implicit def jsonDecoder[F[_]: Sync, A <: Product: Decoder]: EntityDecoder[F, A] = jsonOf[F, A]
//    implicit def jsonEncoder[F[_]: Sync, A <: Product: Encoder]: EntityEncoder[F, A] = jsonEncoderOf[F, A]
//}



object ScalaRoute {

    def DataParserRoutes[F[_]: Sync](C: DataParser[F]): HttpRoutes[F] = {
        val dsl = new Http4sDsl[F]{}
        import dsl._
        var source_format = ""
        var dest_format = ""
        HttpRoutes.of[F] {
            case req @ POST -> Root / "parser" =>
                //Find Source and Destination Format from Request Parameter
                for (param <- req.multiParams) {
                    param._1 match {
                        case "source" => source_format = param._2.head.toString()
                        case "dest" => dest_format = param._2.head.toString()
                    }
                }
                //Get JSON from Request Body as String
//                val originalSource = req.bodyAsText.map(_.toVector.mkString)
//                val r = ""
                Ok(req.as[String].map(str => C.JsonParser(DataParser.OriginData(str))))
//                val response = req.as[String].map(str => C.JsonParser(str))
//                println(r)
//                val RequestText = C.JsonParser(req.as[String])

//                Ok(r)
        }
    }

    def f[T](v: T) = v match {
        case _: Int    => "Int"
        case _: String => "String"
        case _         => "Unknown"
    }

}
