abstract class MessageObject{
    def messageCode: String
    def messageHeader: String
}

object MessageEngine{
    implicit def ResultMessage(msg: String) = new MessageObject {
        override def messageCode: String = "200"
        override def messageHeader: String = msg
    }
    implicit def ParseIntError = new MessageObject {
        override def messageCode: String = "P301"
        override def messageHeader: String = "Parse Int Error"
    }
    implicit def ParseDoubleError = new MessageObject {
        override def messageCode: String = "P302"
        override def messageHeader: String = "Parse Double Error"
    }
    implicit def InvalidCountryCodeError = new MessageObject {
        override def messageCode: String = "E001"
        override def messageHeader: String = "Invalid Country Code"
    }
    implicit def ResultNotFoundError = new MessageObject {
        override def messageCode: String = "N001"
        override def messageHeader: String = "No result is found"
    }
    implicit def InvalidEmployeeID = new MessageObject {
        override def messageCode: String = "E002"
        override def messageHeader: String = "Invalid Employee ID"
    }
    implicit def InvalidEmployeeName = new MessageObject {
        override def messageCode: String = "E003"
        override def messageHeader: String = "Invalid Employee Name"
    }
    implicit def InvalidEmployeeEmail = new MessageObject {
        override def messageCode: String = "E004"
        override def messageHeader: String = "Invalid Employee Email"
    }
    implicit def InvalidEmployeeGender = new MessageObject {
        override def messageCode: String = "E005"
        override def messageHeader: String = "Invalid Employee Gender"
    }
    implicit def InvalidEmployeeIP = new MessageObject {
        override def messageCode: String = "E006"
        override def messageHeader: String = "Invalid Employee IP"
    }
}

