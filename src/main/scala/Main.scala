
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._

object Main extends IOApp {

    //Setting up Server
    def run(args: List[String]) =
        ScalaServer.stream[IO].compile.drain.as(ExitCode.Success)


}